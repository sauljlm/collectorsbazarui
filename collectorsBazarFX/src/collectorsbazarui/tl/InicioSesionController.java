package collectorsbazarui.tl;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;
import java.time.LocalDate;

public class InicioSesionController {
    Gestor gestor;

    @FXML
    private TextField correo, contrasenia;

    @FXML
    private ComboBox comboBox = new ComboBox();

    @FXML
    public void initialize() {
        this.gestor = new Gestor();
        comboBox.getItems().addAll("Administrador","Coleccionista","Vendedor");
    }

    /**
     * Obtiene los datos del formulario FX y los envia al gestor para iniciar sesion
     * @param actionEvent
     */
    public void iniciarSesion(ActionEvent actionEvent) {
        if(correo.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su correo");
            return;
        }

        try {
            gestor.inicioSesion(correo.getText(),contrasenia.getText());
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                showAlert(Alert.AlertType.CONFIRMATION, "", "Sesion iniciada exitosamente!");
                if (usuarioLogueado.getTipoUsuario() == 1) {
                    goToAdministradorDash(actionEvent);
                } else if (usuarioLogueado.getTipoUsuario() == 2) {
                    goToColeccionistaDash(actionEvent);
                } else if (usuarioLogueado.getTipoUsuario() == 3) {
                    goToVendedorDash(actionEvent);
                }
            }
        }
        catch (Exception e){
            showAlert(Alert.AlertType.ERROR, "Error de Registro!", e.getMessage());
        }

        correo.clear();
        contrasenia.clear();
    }

    /**
     * Obtiene el tipo de usuario a registrar
     * @param actionEvent
     */
    public void getUserType(ActionEvent actionEvent) {
        switch (String.valueOf(comboBox.getValue())) {
            case "Administrador":
                goToAdministrador(actionEvent);
                break;
            case "Coleccionista":
                goToColeccionista(actionEvent);
                break;
            case "Vendedor":
                goToVendedor(actionEvent);
                break;
            default:
                System.out.println("Tipo de usuario incorrecto");
        }
    }

    /**
     * Ir a vista administrador dash
     * @param event
     */
    public void goToAdministradorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("admindashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista coleccionista dash
     * @param event
     */
    public void goToColeccionistaDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("coleccionistadashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista vendedor dash
     * @param event
     */
    public void goToVendedorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("vendedorshboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear vendedor
     * @param event
     */
    public void goToVendedor(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("newvendedor.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear coleccionista
     * @param event
     */
    public void goToColeccionista(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("newcoleccionista.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear administrador
     * @param event
     */
    public void goToAdministrador(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("newadministrador.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
