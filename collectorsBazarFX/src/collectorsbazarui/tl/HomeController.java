package collectorsbazarui.tl;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.subasta.Subasta;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class HomeController {
    Gestor gestor;

    @FXML
    private Button goToDashBTN;
    @FXML
    private ComboBox comboBox = new ComboBox();
    @FXML
    TableView<Subasta> listSubastas;
    public ObservableList<Subasta> subastas;
    @FXML
    private TableColumn<Subasta, String> tCodigo;
    @FXML
    private TableColumn<Subasta, String> tItem;
    @FXML
    private TableColumn<Subasta, String> tPrecio;
    @FXML
    private TableColumn<Subasta, String> tOfertante;
    @FXML
    private TableColumn<Subasta, String> tOferta;
    @FXML
    private TableColumn<Subasta, String> tVendedor;
    @FXML
    private TableColumn<Subasta, String> tOfertas;

    public void initialize() throws SQLException, ClassNotFoundException {
        this.gestor = new Gestor();
        comboBox.getItems().addAll("Administrador","Coleccionista","Vendedor");
        if (gestor.checkearUsuarioLogueado() == null) {
            goToDashBTN.setVisible(false);
        } else {
            goToDashBTN.setVisible(true);
        }

        if (gestor.listarSubastas() != null) {
            actualizarDatos();
        }
    }

    /**
     * Actualiza los datos de la tabla subasta en pantalla, obtiene los datos del gestor
     */
    public void actualizarDatos() {
        try {
            ArrayList<Subasta> finalSubastas = new ArrayList<>();
            for (Subasta tmpSubasta: gestor.listarSubastas()) {
                if (!tmpSubasta.getEstadoSubasta().equals("cerrada")) {
                    finalSubastas.add(tmpSubasta);
                }
            }
            subastas = FXCollections.observableArrayList(finalSubastas);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tCodigo.setCellValueFactory(new PropertyValueFactory<Subasta, String>("identificacion"));
        tItem.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getItem().getNombre()));
        tPrecio.setCellValueFactory(new PropertyValueFactory<Subasta, String>("precioMinimo"));
        tOfertante.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getOfertante()));
        tOferta.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecioOferta()));
        tVendedor.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getVendedor().getNombre()));
        tOfertas.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNOfertas()));

        listSubastas.setItems(subastas);
    }

    @FXML
    private void seleccionarSubasta(MouseEvent e) {
        Subasta subastaSeleccionada = listSubastas.getSelectionModel().getSelectedItem();
        Navigation.setSubastaSeleccionada(subastaSeleccionada);
        goToViewSubasta(e);
    }

    /**
     * Obtiene el tipo de usuario a registrar
     * @param actionEvent
     */
    public void getUserType(ActionEvent actionEvent) {
        switch (String.valueOf(comboBox.getValue())) {
            case "Administrador":
                goToAdministrador(actionEvent);
                break;
            case "Coleccionista":
                goToColeccionista(actionEvent);
                break;
            case "Vendedor":
                goToVendedor(actionEvent);
                break;
            default:
                System.out.println("Tipo de usuario incorrecto");
        }
    }

    /**
     * Ir a vista inicio sesion
     * @param event
     */
    public void goToInicioSesion(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("iniciosesion.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear vendedor
     * @param event
     */
    public void goToVendedor(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("newvendedor.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear coleccionista
     * @param event
     */
    public void goToColeccionista(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("newcoleccionista.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear administrador
     * @param event
     */
    public void goToAdministrador(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("newadministrador.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear nuevo item
     * @param event
     */
    public void newItem(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                if (usuarioLogueado.getTipoUsuario() == 1) {
                    showAlert(Alert.AlertType.ERROR, "Error!", "No puedes registrar un Item como Administrador");
                } else if (usuarioLogueado.getTipoUsuario() == 2 || usuarioLogueado.getTipoUsuario() == 3) {
                    goToNewItem(event);
                }
            } else {
                showAlert(Alert.AlertType.ERROR, "Error!", "Debe iniciar sesion para agregar un nuevo item");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear nuevo item
     * @param event
     */
    public void goToNewItem(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                Parent root = FXMLLoader.load(getClass().getResource("newitem.fxml"));
                Scene scene = new Scene(root);
                Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                appStage.setScene(scene);
                appStage.toFront();
                appStage.show();
            } else {
                showAlert(Alert.AlertType.ERROR, "Error!", "Debe iniciar sesion para agregar un nuevo item");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear nueva Subasta
     * @param event
     */
    public void newSubasta(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado.getTipoUsuario() == 1) {
                showAlert(Alert.AlertType.ERROR, "Error!", "No puedes una subasta como Administrador");
            } else if (usuarioLogueado.getTipoUsuario() == 2 || usuarioLogueado.getTipoUsuario() == 3) {
                goToNewSubasta(event);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear nueva Subasta
     * @param event
     */
    public void goToNewSubasta(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                Parent root = FXMLLoader.load(getClass().getResource("newsubasta.fxml"));
                Scene scene = new Scene(root);
                Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                appStage.setScene(scene);
                appStage.toFront();
                appStage.show();
            } else {
                showAlert(Alert.AlertType.ERROR, "Error!", "Debe iniciar sesion para crear una subasta");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtiene el tipo de usuario logueado y llama a la funcion goToUsuarioDash
     * @param actionEvent
     */
    public void goToDash(ActionEvent actionEvent) {
        Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
        if (usuarioLogueado.getTipoUsuario() == 1) {
            goToAdministradorDash(actionEvent);
        } else if (usuarioLogueado.getTipoUsuario() == 2) {
            goToColeccionistaDash(actionEvent);
        } else if (usuarioLogueado.getTipoUsuario() == 3) {
            goToVendedorDash(actionEvent);
        }
    }

    /**
     * Ir a vista administrador dash
     * @param event
     */
    public void goToAdministradorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("admindashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista coleccionista dash
     * @param event
     */
    public void goToColeccionistaDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("coleccionistadashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista vendedor dash
     * @param event
     */
    public void goToVendedorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("vendedorshboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista ver Subasta
     * @param event
     */
    public void goToViewSubasta(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("subastaview.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }
}