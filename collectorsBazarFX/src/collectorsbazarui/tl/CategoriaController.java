package collectorsbazarui.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import lopez.saul.bl.entities.categoria.Categoria;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

public class CategoriaController {

    Gestor gestor;

    @FXML
    private TextField nombre;
    @FXML
    TableView<Categoria> listCategorias;
    @FXML
    private TableColumn<Usuario, String> tNombre;
    @FXML
    private TableColumn<Usuario, Integer> tCodigoNumerico;
    @FXML
    private TableColumn<Usuario, String> tEstado;

    public ObservableList<Categoria> categorias;

    @FXML
    public void initialize() {
        this.gestor = new Gestor();
        cargarCategorias();
    }

    public void cargarCategorias() {
        try {
            categorias = FXCollections.observableArrayList(gestor.listarCategorias());
        } catch (Exception e) {
            e.printStackTrace();
        }

        tNombre.setCellValueFactory(new PropertyValueFactory<Usuario, String>("nombre"));
        tCodigoNumerico.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("codigoNumerico"));
        tEstado.setCellValueFactory(new PropertyValueFactory<Usuario, String>("estado"));

        listCategorias.setItems(categorias);
    }

    /**
     * Obtiene los datos del formulario FX y los envia al gestor para registrar
     * @param actionEvent
     */
    public void registrar(ActionEvent actionEvent) {
        if(nombre.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese el nombre de la categoria");
            return;
        }

        try {
            gestor.insertarCategoria(nombre.getText());
            cargarCategorias();
            showAlert(Alert.AlertType.CONFIRMATION, "Categoria registrada exitosamente!", "Categoria " + nombre.getText() + " ingresada");
        }
        catch (Exception e){
            showAlert(Alert.AlertType.ERROR, "Error de Registro!", "se produjo el siguiente error: " + e.getMessage());
        }

        nombre.clear();
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("admindashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
