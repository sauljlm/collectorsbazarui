package collectorsbazarui.tl;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

import java.time.LocalDate;

public class ViewItemsController {
    Gestor gestor;

    public ObservableList<Item> items;
    @FXML
    TableView<Item> listItems;
    @FXML
    private TableColumn<Item, String> tNombre;
    @FXML
    private TableColumn<Item, String> tDescripcion;
    @FXML
    private TableColumn<Item, String> tEstado;
    @FXML
    private TableColumn<Item, LocalDate> tFechaCompra;
    @FXML
    private TableColumn<Item, String> tAntiguedad;
    @FXML
    private TableColumn<Item, String> tCategoria;
    @FXML
    private TableColumn<Item, String> tIdentificacion;

    public void initialize() {
        this.gestor = new Gestor();

        try {
            items = FXCollections.observableArrayList(gestor.listarItems());
        } catch (Exception e) {
            e.printStackTrace();
        }

        tNombre.setCellValueFactory(new PropertyValueFactory<Item, String>("nombre"));
        tDescripcion.setCellValueFactory(new PropertyValueFactory<Item, String>("descripcion"));
        tEstado.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstado().getNombre()));
        tFechaCompra.setCellValueFactory(new PropertyValueFactory<Item, LocalDate>("fechaCompra"));
        tAntiguedad.setCellValueFactory(new PropertyValueFactory<Item, String>("antiguedad"));
        tCategoria.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategoria().getNombre()));
        tIdentificacion.setCellValueFactory(new PropertyValueFactory<Item, String>("identificacion"));

        listItems.setItems(items);
    }

    public void goToDash(ActionEvent actionEvent) {
        Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
        if (usuarioLogueado.getTipoUsuario() == 1) {
            goToAdministradorDash(actionEvent);
        } else if (usuarioLogueado.getTipoUsuario() == 2) {
            goToColeccionistaDash(actionEvent);
        } else if (usuarioLogueado.getTipoUsuario() == 3) {
            goToVendedorDash(actionEvent);
        }
    }

    /**
     * Ir a vista administrador dash
     * @param event
     */
    public void goToAdministradorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("admindashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista coleccionista dash
     * @param event
     */
    public void goToColeccionistaDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("coleccionistadashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista vendedor dash
     * @param event
     */
    public void goToVendedorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("vendedorshboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}