package collectorsbazarui.tl;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lopez.saul.bl.entities.usuario.Administrador;
import lopez.saul.bl.entities.usuario.Coleccionista;
import lopez.saul.bl.logic.Gestor;

public class AdministradorDashController {
    Gestor gestor;
    @FXML
    private Text nombre,direccion,correo;

    public void initialize() {
            this.gestor = new Gestor();
            actualizadDatos();
    }

    public void actualizadDatos() {
        Administrador administrador = (Administrador) gestor.checkearUsuarioLogueado();
        nombre.setText(administrador.getNombre());
        correo.setText(administrador.getCorreo());
        direccion.setText(administrador.getDireccion());
    }

    /**
     * Cerrar sesion
     * @param event
     */
    public void cerrarSesion(Event event) {
        backToHome(event);
        gestor.cerrarSesion();
    }

    /**
     * Ir a vista categoria
     * @param event
     */
    public void goToCategorias(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("categoria.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista estado
     * @param event
     */
    public void goToEstados(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("estado.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista ver usuarios
     * @param event
     */
    public void goToViewUsuarios(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("viewusuarios.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista ver items
     * @param event
     */
    public void goToViewItems(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("viewitems.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista ver subastas
     * @param event
     */
    public void goToDubastas(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("viewsubastas.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
