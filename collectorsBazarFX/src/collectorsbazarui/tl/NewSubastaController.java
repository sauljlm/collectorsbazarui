package collectorsbazarui.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class NewSubastaController {
    Gestor gestor;

    @FXML
    private TextField precioMinino,hora;

    @FXML
    private Text itemsPreview;

    @FXML
    private DatePicker fechaInicio,fechaVende;

    @FXML
    private ComboBox comboBox = new ComboBox();

    public ObservableList<Item> tmpItems;
    public ArrayList<Item> itemsSeleccionados = new ArrayList<>();

    @FXML
    public void initialize() throws SQLException, ClassNotFoundException {
        this.gestor = new Gestor();
        generarItems();
    }

    /**
     * Obtiene los items del usuario logueado y los pone en el combobox
     */
    public void generarItems() {
        try {
            ArrayList<String> itemsIDs = gestor.listarUsuarioItems();
            ArrayList<Item> itemsData = gestor.listarItems();
            ArrayList<Item> finalItems = new ArrayList<>();

            for (Item tmpItem:itemsData) {
                for (String itemId: itemsIDs) {
                    if (tmpItem.getIdentificacion().equals(itemId)) {
                        finalItems.add(tmpItem);
                    }
                }
            }
            tmpItems = FXCollections.observableArrayList(finalItems);
            comboBox.setItems(tmpItems);
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Error en el servidor!", "No se encontraron Items");
        }
    }

    public void addItem(ActionEvent actionEvent) {
        Item itemSelected = (Item) comboBox.getValue();
        itemsSeleccionados.add(itemSelected);
        String todoItems = "";
        for (Item item:itemsSeleccionados) {
            todoItems = todoItems + item.getNombre() + ", ";
        }
        itemsPreview.setText(todoItems);
    }

    /**
     * Obtiene los datos del formulario FX y los envia al gestor para registrar
     * @param actionEvent
     */
    public void registrar(ActionEvent actionEvent) {
        if(precioMinino.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su nombre");
            return;
        }
        LocalDate date = fechaInicio.getValue();
        if (date == null) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese la fecha de compra");
            return;
        }
        if(hora.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su nombre");
            return;
        }
        LocalDate endDate = fechaVende.getValue();
        if (endDate == null) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese la fecha de compra");
            return;
        }
        if(comboBox.getSelectionModel().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor seleccione uno o mas items");
            return;
        }

        try {
            if(gestor.insertarSubasta(fechaInicio.getValue(),hora.getText(),fechaVende.getValue(),Integer.valueOf(precioMinino.getText()),itemsSeleccionados)) {
                showAlert(Alert.AlertType.CONFIRMATION, "", "Subasta registrada exitosamente!");
                backToHome(actionEvent);
            } else {
                showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "No fue posible registrar la subasta");
            }
        }
        catch (Exception e){
            showAlert(Alert.AlertType.ERROR, "Error de Registro!", "se produjo el siguiente error: " + e.getMessage());
        }

        precioMinino.clear();
    }

    /**
     * Ir a vista crear nuevo item
     * @param event
     */
    public void goToNewItem(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                Parent root = FXMLLoader.load(getClass().getResource("newitem.fxml"));
                Scene scene = new Scene(root);
                Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                appStage.setScene(scene);
                appStage.toFront();
                appStage.show();
            } else {
                showAlert(Alert.AlertType.ERROR, "Error!", "Debe iniciar sesion para agregar un nuevo item");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
