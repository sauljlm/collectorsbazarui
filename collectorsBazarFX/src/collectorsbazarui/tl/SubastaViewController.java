package collectorsbazarui.tl;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.oferta.Oferta;
import lopez.saul.bl.entities.ordencompra.OrdenCompra;
import lopez.saul.bl.entities.ordencompra.OrdenCompraDAO;
import lopez.saul.bl.entities.subasta.Subasta;
import lopez.saul.bl.entities.usuario.Coleccionista;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

import javax.print.attribute.standard.MediaSize;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SubastaViewController {
    Gestor gestor;
    Subasta subasta;
    @FXML
    private Text estado,precioMinimo,moderador,codigo,fechaCreacion,fechaInicio,horaInicio,fechaVencimiento,vendedor,puntuacion,cantidadOfertas,nuevaOfertaTitulo,elegirGanadorTitulo;
    @FXML
    private TextField montoOferta;
    @FXML
    private Button agregarSubasta,elegirGanador;

    public ObservableList<Item> items;
    @FXML
    TableView<Item> listItems;
    @FXML
    private TableColumn<Item, String> tNombreItem;
    @FXML
    private TableColumn<Item, String> tDescripcion;
    @FXML
    private TableColumn<Item, String> tEstado;
    @FXML
    private TableColumn<Item, LocalDate> tFechaCompra;
    @FXML
    private TableColumn<Item, String> tAntiguedad;
    @FXML
    private TableColumn<Item, String> tCategoria;
    @FXML
    private TableColumn<Item, String> tIdentificacion;

    @FXML
    TableView<Oferta> listOfertas;
    public ObservableList<Oferta> ofertas;
    @FXML
    private TableColumn<Oferta, String> tNombreOferta;
    @FXML
    private TableColumn<Oferta, String> tPuntuacionOferta;
    @FXML
    private TableColumn<Oferta, String> tPrecioOferta;
    @FXML
    private TableColumn<Oferta, LocalDate> tFechaOferta;

    public void initialize() {
        gestor = new Gestor();
        subasta = Navigation.getSubastaSeleccionada();
        actualizarVentana();
    }

    public void actualizarVentana() {
        controlarOpciones();
        actualizarDatos();
        actualizarItems();
        actualizarOfertas();
    }

    public void controlarOpciones() {
        boolean mostrar = true;
        elegirGanadorTitulo.setVisible(false);
        elegirGanador.setVisible(false);
        if (gestor.checkearUsuarioLogueado() == null) {
            mostrar = false;
        } else if (gestor.checkearUsuarioLogueado().getTipoUsuario() != 2) {
            mostrar = false;
        } else if (gestor.checkearUsuarioLogueado().getCorreo().equals(subasta.getNombreVendedor())) {
            mostrar = false;
        } else if (gestor.checkearUsuarioLogueado().getCorreo().equals(subasta.getModerador())) {
            mostrar = false;
            elegirGanadorTitulo.setVisible(true);
            elegirGanador.setVisible(true);
        }
        montoOferta.setVisible(mostrar);
        agregarSubasta.setVisible(mostrar);
        nuevaOfertaTitulo.setVisible(mostrar);
    }

    public void actualizarDatos() {
        estado.setText(subasta.getEstadoSubasta());
        precioMinimo.setText(String.valueOf(subasta.getPrecioMinimo()));
        moderador.setText(gestor.obtenerColeccionista(subasta.getModerador()).getNombre());
        codigo.setText(subasta.getIdentificacion());
        fechaCreacion.setText(String.valueOf(subasta.getFechaCreacion()));
        fechaInicio.setText(String.valueOf(subasta.getFechaInicio()));
        horaInicio.setText(subasta.getHoraInicio());
        fechaVencimiento.setText(String.valueOf(subasta.getFechaVence()));
        vendedor.setText(gestor.obtenerDuenio(subasta.getNombreVendedor()).getNombre());
        puntuacion.setText(String.valueOf(subasta.getPuntuacion()));
        try {
            cantidadOfertas.setText(String.valueOf(gestor.listarSubastaOfertas(subasta.getIdentificacion()).size()));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void actualizarItems() {
        try {
            ArrayList<Item> itemsData = gestor.listarSubastaItems(subasta.getIdentificacion());
            items = FXCollections.observableArrayList(itemsData);
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Error en el servidor!", "No se encontraron Items");
        }

        tNombreItem.setCellValueFactory(new PropertyValueFactory<Item, String>("nombre"));
        tDescripcion.setCellValueFactory(new PropertyValueFactory<Item, String>("descripcion"));
        tEstado.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstado().getNombre()));
        tFechaCompra.setCellValueFactory(new PropertyValueFactory<Item, LocalDate>("fechaCompra"));
        tAntiguedad.setCellValueFactory(new PropertyValueFactory<Item, String>("antiguedad"));
        tCategoria.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategoria().getNombre()));
        tIdentificacion.setCellValueFactory(new PropertyValueFactory<Item, String>("identificacion"));

        listItems.setItems(items);
    }

    public void actualizarOfertas() {
        try {
            ArrayList<Oferta> ofertasData = gestor.listarSubastaOfertas(subasta.getIdentificacion());
            ofertas = FXCollections.observableArrayList(ofertasData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tNombreOferta.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getOfertante().getNombre()));
        tPuntuacionOferta.setCellValueFactory(new PropertyValueFactory<Oferta, String>("puntuacion"));
        tPrecioOferta.setCellValueFactory(new PropertyValueFactory<Oferta, String>("precioOferta"));
        tFechaOferta.setCellValueFactory(new PropertyValueFactory<Oferta, LocalDate>("fechaOferta"));

        listOfertas.setItems(ofertas);
    }

    @FXML
    private void seleccionarItem(MouseEvent e) {
        Item itemSeleccionado = listItems.getSelectionModel().getSelectedItem();
        System.out.println(itemSeleccionado);
        // Navigation.setItemSeleccionado(itemSeleccionado);
        // goToViewItem(e);
    }

    /**
     * Ir a vista crear nueva oferta
     * @param event
     */
    public void NewOferta(Event event) {
        if(montoOferta.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error!", "Por favor ingrese el monto de la oferta");
        } else if(Integer.parseInt(montoOferta.getText()) <= Integer.parseInt(subasta.getPrecioOferta()) || Integer.parseInt(montoOferta.getText()) <= subasta.getPrecioMinimo()) {
            showAlert(Alert.AlertType.ERROR, "Error!", "Tu oferta debe ser mayor a " + subasta.getPrecioOferta());
        } else {
            Coleccionista usuarioLogueado = (Coleccionista) gestor.checkearUsuarioLogueado();
            try {
                gestor.insertarOferta(subasta,usuarioLogueado,Integer.parseInt(montoOferta.getText()));
                showAlert(Alert.AlertType.CONFIRMATION, "Error de Formulario!", "Oferta registrada!");
                actualizarVentana();
            } catch (Exception e) {
                showAlert(Alert.AlertType.ERROR, "Error de Formulario!", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * Elige al ganador y almacena los datos en Navigation
     * @param e
     */
    public void elegirGanador(ActionEvent e) {
        try{
            Oferta ultimaOferta = subasta.getUltimaOferta();
            ArrayList<Item> itemsData = gestor.listarSubastaItems(subasta.getIdentificacion());
            gestor.insertarOrdenCompra(subasta, ultimaOferta.getNombre(),ultimaOferta.getFechaOferta(),"Detalle", itemsData, ultimaOferta.getPrecioOferta());
            for (Item tmpItem: itemsData) {
                gestor.agregarNuevoUsuarioItem(gestor.obtenerDuenio(ultimaOferta.getNombre()), tmpItem.getIdentificacion());
                gestor.eliminarUsuarioItem(gestor.obtenerDuenio(subasta.getNombreVendedor()), tmpItem.getIdentificacion());
                showAlert(Alert.AlertType.CONFIRMATION, subasta.getIdentificacion(), "Orden de compra registrada!");
                gestor.actualizarSubasta(subasta, "cerrada");
                OrdenCompra ordenCompra = null;
                for (OrdenCompra tmpordenCompra: gestor.listarOrdenCompras()) {
                    if (tmpordenCompra.getNombre().equals(ultimaOferta.getNombre())) {
                        ordenCompra = tmpordenCompra;
                    }
                }
                Navigation.setOrdenCompraSeleccionada(ordenCompra);
                Navigation.setSubastaOrdenCompra(subasta);
            }
        } catch (Exception exception) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", exception.getMessage());
        }
    }

    /**
     * Ir a vista item
     * @param event
     */
    public void goToViewItem(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                Parent root = FXMLLoader.load(getClass().getResource("itemview.fxml"));
                Scene scene = new Scene(root);
                Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                appStage.setScene(scene);
                appStage.toFront();
                appStage.show();
            } else {
                showAlert(Alert.AlertType.ERROR, "Error!", "Debe iniciar sesion para agregar un nuevo item");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
