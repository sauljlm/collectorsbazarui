package collectorsbazarui.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lopez.saul.bl.entities.categoria.Categoria;
import lopez.saul.bl.entities.estado.Estado;
import lopez.saul.bl.entities.imagen.Imagen;
import lopez.saul.bl.logic.Gestor;

import javax.imageio.ImageIO;
import javax.security.auth.callback.Callback;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class NewItemController {
    Gestor gestor;

    @FXML
    private TextField nombre, descripcion;

    @FXML
    private DatePicker fechaCompra;

    @FXML
    private ImageView imagenView;

    @FXML
    private ComboBox estado,categoria;

    public ObservableList<Estado> estados;
    public ObservableList<Categoria> categorias;

    @FXML
    public void initialize() throws SQLException, ClassNotFoundException {
        this.gestor = new Gestor();
        generarEstados();
        generarCategorias();
    }

    public void generarEstados() throws SQLException, ClassNotFoundException {
        estados = FXCollections.observableArrayList(gestor.listarEstados());
        estado.setItems(estados);
    }

    public void generarCategorias() throws SQLException, ClassNotFoundException {
        categorias = FXCollections.observableArrayList(gestor.listarCategorias());
        categoria.setItems(categorias);
    }

    /**
     * Obtiene los datos del formulario FX y los envia al gestor para registrar
     * @param actionEvent
     */
    public void registrar(ActionEvent actionEvent) {
        if(nombre.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su nombre");
            return;
        }
        if(descripcion.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese la descripcion");
            return;
        }
        LocalDate date = fechaCompra.getValue();
        if (date == null) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese la fecha de compra");
            return;
        }
        if(estado.getSelectionModel().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor seleccione el estado");
            return;
        }
        if(categoria.getSelectionModel().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor seleccione la categoria");
            return;
        }

        Estado estadoSelected = (Estado) estado.getValue();
        Categoria categoriaSelected = (Categoria) categoria.getValue();

        try {
            if(gestor.insertarItem(nombre.getText(),descripcion.getText(),estadoSelected,fechaCompra.getValue(),categoriaSelected)) {
                showAlert(Alert.AlertType.CONFIRMATION, "Usuario registrado exitosamente!", "Usuario " + nombre.getText() + " ingresado");
                backToHome(actionEvent);
            } else {
                showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "El usuario coleccionista ya existe");
            }
        }
        catch (Exception e){
            showAlert(Alert.AlertType.ERROR, "Error de Registro!", "se produjo el siguiente error: " + e.getMessage());
        }

        nombre.clear();
    }

    /**
     * Carga la imagen de la interfaz y la envia al gestor
     * @param actionEvent
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void cargarImagen(ActionEvent actionEvent) throws IOException, SQLException, ClassNotFoundException {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);
        BufferedImage bufferedImage = ImageIO.read(file);
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        imagenView.setImage(image);
        Imagen imagen = new Imagen();
        imagen.setIdentificacion(gestor.obtenerCodigoImagen());
        imagen.setImagen(new FileInputStream(file));
        gestor.insertarImagen(imagen);
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
