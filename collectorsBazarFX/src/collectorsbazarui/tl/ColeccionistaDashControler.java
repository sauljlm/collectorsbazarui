package collectorsbazarui.tl;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.ordencompra.OrdenCompra;
import lopez.saul.bl.entities.ordencompra.OrdenCompraDAO;
import lopez.saul.bl.entities.subasta.Subasta;
import lopez.saul.bl.entities.usuario.Coleccionista;
import lopez.saul.bl.entities.usuario.ColeccionistaDAO;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ColeccionistaDashControler {
    Gestor gestor;

    @FXML
    private Text nombre,correo,direccion,puntuacion,intereses;
    public ObservableList<Item> items;
    @FXML
    TableView<Item> listItems;
    @FXML
    private TableColumn<Item, String> tNombre;
    @FXML
    private TableColumn<Item, String> tDescripcion;
    @FXML
    private TableColumn<Item, String> tEstado;
    @FXML
    private TableColumn<Item, String> tAntiguedad;
    @FXML
    private TableColumn<Item, String> tCategoria;

    @FXML
    TableView<Subasta> listSubastas;
    public ObservableList<Subasta> subastas;
    @FXML
    private TableColumn<Subasta, String> tCodigo;
    @FXML
    private TableColumn<Subasta, String> tItem;
    @FXML
    private TableColumn<Subasta, String> tPrecio;
    @FXML
    private TableColumn<Subasta, String> tOfertante;
    @FXML
    private TableColumn<Subasta, String> tOferta;
    @FXML
    private TableColumn<Subasta, String> tVendedor;
    @FXML
    private TableColumn<Subasta, String> tOfertas;

    public void initialize() {
        this.gestor = new Gestor();
        actualizadDatos();
        actualizarItems();
        actualizarSubastas();
    }

    public void checkOrdenCompra(ActionEvent event) {
        try {
            for(OrdenCompra tmpOrden : gestor.listarOrdenCompras()) {
                if (tmpOrden.getNombre().equals(gestor.checkearUsuarioLogueado().getCorreo())) {
                    goToOrdenCompra(event);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void actualizadDatos() {
        Coleccionista coleccionista = (Coleccionista) gestor.checkearUsuarioLogueado();
        nombre.setText(coleccionista.getNombre());
        correo.setText(coleccionista.getCorreo());
        direccion.setText(coleccionista.getDireccion());
        puntuacion.setText(String.valueOf(coleccionista.getPuntuacion()));

        String todoIntereses = "";
        try {
            for (String interes: gestor.listarIntereses(coleccionista.getCorreo())) {
                todoIntereses = todoIntereses + interes;
            }
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Error en el servidor!", "No se encontraron Intereses");
        }
        intereses.setText(todoIntereses);
    }

    public void actualizarItems() {
        try {
            ArrayList<String> itemsIDs = gestor.listarUsuarioItems();
            ArrayList<Item> itemsData = gestor.listarItems();
            ArrayList<Item> finalItems = new ArrayList<>();

            for (Item tmpItem:itemsData) {
                for (String itemId: itemsIDs) {
                    if (tmpItem.getIdentificacion().equals(itemId)) {
                        finalItems.add(tmpItem);
                    }
                }
            }

            items = FXCollections.observableArrayList(finalItems);
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Error en el servidor!", "No se encontraron Items");
        }

        tNombre.setCellValueFactory(new PropertyValueFactory<Item, String>("nombre"));
        tDescripcion.setCellValueFactory(new PropertyValueFactory<Item, String>("descripcion"));
        tEstado.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstado().getNombre()));
        tAntiguedad.setCellValueFactory(new PropertyValueFactory<Item, String>("antiguedad"));
        tCategoria.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategoria().getNombre()));

        listItems.setItems(items);
    }

    public void actualizarSubastas() {
        try {
            ArrayList<Subasta> finalSubastas = new ArrayList<>();
            for (Subasta tmpSubasta: gestor.listarSubastas()) {
                if (!tmpSubasta.getEstadoSubasta().equals("cerrada")) {
                    finalSubastas.add(tmpSubasta);
                }
            }
            subastas = FXCollections.observableArrayList(finalSubastas);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tCodigo.setCellValueFactory(new PropertyValueFactory<Subasta, String>("identificacion"));
        tItem.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getItem().getNombre()));
        tPrecio.setCellValueFactory(new PropertyValueFactory<Subasta, String>("precioMinimo"));
        tOfertante.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getOfertante()));
        tOferta.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecioOferta()));
        tVendedor.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getVendedor().getNombre()));
        tOfertas.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNOfertas()));

        listSubastas.setItems(subastas);
    }

    @FXML
    private void seleccionarSubasta(MouseEvent e) {
        Subasta subastaSeleccionada = listSubastas.getSelectionModel().getSelectedItem();
        Navigation.setSubastaSeleccionada(subastaSeleccionada);
        goToViewSubasta(e);
    }

    /**
     * Ir a vista ver Subasta
     * @param event
     */
    public void goToViewSubasta(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("subastaview.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista ver Subasta
     * @param event
     */
    public void goToOrdenCompra(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("ordencompra.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Cerrar sesion
     * @param event
     */
    public void cerrarSesion(Event event) {
        gestor.cerrarSesion();
        backToHome(event);
    }

    /**
     * Ir a vista crear nueva Subasta
     * @param event
     */
    public void goToNewSubasta(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("newsubasta.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
