package collectorsbazarui.tl;

import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.ordencompra.OrdenCompra;
import lopez.saul.bl.entities.subasta.Subasta;

public class Navigation {
    public static Subasta subastaSeleccionada;
    public static OrdenCompra ordenCompraSeleccionada;
    public static Subasta subastaOrdenCompra;

    public Navigation() {
    }

    public static Subasta getSubastaSeleccionada() {
        return subastaSeleccionada;
    }

    public static void setSubastaSeleccionada(Subasta subastaSeleccionada) {
        Navigation.subastaSeleccionada = subastaSeleccionada;
    }

    public static OrdenCompra getOrdenCompraSeleccionada() {
        return ordenCompraSeleccionada;
    }

    public static void setOrdenCompraSeleccionada(OrdenCompra ordenCompra) {
        Navigation.ordenCompraSeleccionada = ordenCompra;
    }

    public static Subasta getSubastaOrdenCompra() {
        return subastaOrdenCompra;
    }

    public static void setSubastaOrdenCompra(Subasta subastaOrdenCompra) {
        Navigation.subastaOrdenCompra = subastaOrdenCompra;
    }
}
