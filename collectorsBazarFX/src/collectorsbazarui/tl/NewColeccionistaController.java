package collectorsbazarui.tl;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lopez.saul.bl.logic.Gestor;
import java.time.LocalDate;

public class NewColeccionistaController {

    Gestor gestor;

    @FXML
    private TextField nombre, direccion, correo, contrasenia;

    @FXML
    private DatePicker fechaNacimiento;

    @FXML
    public void initialize() {
        this.gestor = new Gestor();
    }

    /**
     * Obtiene los datos del formulario FX y los envia al gestor para registrar
     * @param actionEvent
     */
    public void registrar(ActionEvent actionEvent) {
        if(nombre.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su nombre");
            return;
        }
        if(direccion.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su direccion");
            return;
        }
        if(correo.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su correo");
            return;
        }
        if(contrasenia.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su contraseña");
            return;
        }
        LocalDate date = fechaNacimiento.getValue();
        if (date == null) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su fecha de nacimiento");
            return;
        }
        if (gestor.generarEdad(date) < 18) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "El usuario debe de ser mayor de edad");
            return;
        }

        try {
            if(gestor.insertarColeccionista(nombre.getText(),direccion.getText(),correo.getText(),contrasenia.getText(),fechaNacimiento.getValue())) {
                showAlert(Alert.AlertType.CONFIRMATION, "Usuario registrado exitosamente!", "Usuario " + nombre.getText() + " ingresado");
            } else {
                showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "El usuario coleccionista ya existe");
            }
        }
        catch (Exception e){
            showAlert(Alert.AlertType.ERROR, "Error de Registro!", "se produjo el siguiente error: " + e.getMessage());
        }

        nombre.clear();
        direccion.clear();
        correo.clear();
        contrasenia.clear();
        fechaNacimiento.getEditor().clear();
    }

    /**
     * Ir a vista inicio sesion
     * @param event
     */
    public void goToInicioSesion(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("iniciosesion.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
