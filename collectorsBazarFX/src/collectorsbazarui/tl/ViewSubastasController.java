package collectorsbazarui.tl;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lopez.saul.bl.entities.subasta.Subasta;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

import java.sql.SQLException;
import java.time.LocalDate;

public class ViewSubastasController {
    Gestor gestor;

    @FXML
    TableView<Subasta> listSubastas;
    public ObservableList<Subasta> subastas;
    @FXML
    private TableColumn<Subasta, String> tCodigo;
    @FXML
    private TableColumn<Subasta, String> tItem;
    @FXML
    private TableColumn<Subasta, String> tPrecio;
    @FXML
    private TableColumn<Subasta, String> tModerador;
    @FXML
    private TableColumn<Subasta, String> tOferta;
    @FXML
    private TableColumn<Subasta, LocalDate> tFechaInicio;
    @FXML
    private TableColumn<Subasta, LocalDate> tFechaVence;
    @FXML
    private TableColumn<Subasta, String> tOfertas;
    @FXML
    private TableColumn<Subasta, String> tEstado;

    public void initialize() throws SQLException, ClassNotFoundException {
        this.gestor = new Gestor();

        if (gestor.listarSubastas() != null) {
            actualizarDatos();
        }
    }

    public void actualizarDatos() {
        try {
            subastas = FXCollections.observableArrayList(gestor.listarSubastas());
        } catch (Exception e) {
            e.printStackTrace();
        }

        tCodigo.setCellValueFactory(new PropertyValueFactory<Subasta, String>("identificacion"));
        tItem.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getItem().getNombre()));
        tPrecio.setCellValueFactory(new PropertyValueFactory<Subasta, String>("precioMinimo"));
        tFechaInicio.setCellValueFactory(new PropertyValueFactory<Subasta, LocalDate>("fechaInicio"));
        tFechaVence.setCellValueFactory(new PropertyValueFactory<Subasta, LocalDate>("fechaVence"));
        tOferta.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecioOferta()));
        tModerador.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getModeradorNombre()));
        tOfertas.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNOfertas()));
        tEstado.setCellValueFactory(new PropertyValueFactory<Subasta, String>("estadoSubasta"));

        listSubastas.setItems(subastas);
    }

    @FXML
    private void seleccionarSubasta(MouseEvent e) {
        Subasta subastaSeleccionada = listSubastas.getSelectionModel().getSelectedItem();
        Navigation.setSubastaSeleccionada(subastaSeleccionada);
        goToViewSubasta(e);
    }

    /**
     * Ir a vista ver Subasta
     * @param event
     */
    public void goToViewSubasta(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("subastaview.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * llama la funcion go to dash dependiendo del usuario logueado
     * @param actionEvent
     */
    public void goToDash(ActionEvent actionEvent) {
        Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
        if (usuarioLogueado.getTipoUsuario() == 1) {
            goToAdministradorDash(actionEvent);
        } else if (usuarioLogueado.getTipoUsuario() == 2) {
            goToColeccionistaDash(actionEvent);
        } else if (usuarioLogueado.getTipoUsuario() == 3) {
            goToVendedorDash(actionEvent);
        }
    }

    /**
     * Ir a vista administrador dash
     * @param event
     */
    public void goToAdministradorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("admindashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista coleccionista dash
     * @param event
     */
    public void goToColeccionistaDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("coleccionistadashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista vendedor dash
     * @param event
     */
    public void goToVendedorDash(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("vendedorshboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }
}