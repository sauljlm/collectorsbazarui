package collectorsbazarui.tl;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lopez.saul.bl.logic.Gestor;

public class NewVendedorController {

    Gestor gestor;

    @FXML
    private TextField nombre, direccion, correo;

    @FXML
    public void initialize() {
        this.gestor = new Gestor();
    }

    /**
     * Obtiene los datos del formulario FX y los envia al gestor para registrar
     * @param actionEvent
     */
    public void registrar(ActionEvent actionEvent) {
        if(nombre.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su nombre");
            return;
        }
        if(direccion.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su direccion");
            return;
        }
        if(correo.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "Por favor ingrese su correo");
            return;
        }

        try {
            if(gestor.insertarVendedor(nombre.getText(),direccion.getText(),correo.getText())) {
                showAlert(Alert.AlertType.CONFIRMATION, "Usuario registrado exitosamente!", "Usuario " + nombre.getText() + " ingresado");
            } else {
                showAlert(Alert.AlertType.ERROR, "Error de Formulario!", "El usuario coleccionista ya existe");
            }
        }
        catch (Exception e){
            showAlert(Alert.AlertType.ERROR, "Error de Registro!", "se produjo el siguiente error: " + e.getMessage());
        }

        nombre.clear();
        direccion.clear();
        correo.clear();
    }

    /**
     * Ir a vista inicio sesion
     * @param event
     */
    public void goToInicioSesion(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("iniciosesion.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
