package collectorsbazarui.tl;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.subasta.Subasta;
import lopez.saul.bl.entities.usuario.Coleccionista;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.entities.usuario.Vendedor;
import lopez.saul.bl.logic.Gestor;

import java.time.LocalDate;
import java.util.ArrayList;

public class VendedorDashController {
    Gestor gestor;
    @FXML
    private Text nombre,correo,direccion;

    public ObservableList<Item> items;
    @FXML
    TableView<Item> listItems;
    @FXML
    private TableColumn<Item, String> tNombre;
    @FXML
    private TableColumn<Item, String> tDescripcion;
    @FXML
    private TableColumn<Item, String> tEstado;
    @FXML
    private TableColumn<Item, LocalDate> tFechaCompra;
    @FXML
    private TableColumn<Item, String> tAntiguedad;
    @FXML
    private TableColumn<Item, String> tCategoria;
    @FXML
    private TableColumn<Item, String> tIdentificacion;

    @FXML
    TableView<Subasta> listSubastas;
    public ObservableList<Subasta> subastas;
    @FXML
    private TableColumn<Subasta, String> tCodigo;
    @FXML
    private TableColumn<Subasta, String> tItem;
    @FXML
    private TableColumn<Subasta, String> tOfertante;
    @FXML
    private TableColumn<Subasta, String> tOferta;
    @FXML
    private TableColumn<Subasta, String> tOfertas;

    public void initialize() {
        this.gestor = new Gestor();
        actualizarDatos();
        actualizarItems();
        actualizarSubastas();
    }

    public void actualizarDatos() {
        Vendedor vendedor = (Vendedor) gestor.checkearUsuarioLogueado();
        nombre.setText(vendedor.getNombre());
        correo.setText(vendedor.getCorreo());
        direccion.setText(vendedor.getDireccion());
    }

    public void actualizarItems() {
        try {
            ArrayList<String> itemsIDs = gestor.listarUsuarioItems();
            ArrayList<Item> itemsData = gestor.listarItems();
            ArrayList<Item> finalItems = new ArrayList<>();

            for (Item tmpItem:itemsData) {
                for (String itemId: itemsIDs) {
                    if (tmpItem.getIdentificacion().equals(itemId)) {
                        finalItems.add(tmpItem);
                    }
                }
            }
            items = FXCollections.observableArrayList(finalItems);
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Error en el servidor!", "No se encontraron Items");
        }

        tNombre.setCellValueFactory(new PropertyValueFactory<Item, String>("nombre"));
        tDescripcion.setCellValueFactory(new PropertyValueFactory<Item, String>("descripcion"));
        tEstado.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstado().getNombre()));
        tFechaCompra.setCellValueFactory(new PropertyValueFactory<Item, LocalDate>("fechaCompra"));
        tAntiguedad.setCellValueFactory(new PropertyValueFactory<Item, String>("antiguedad"));
        tCategoria.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategoria().getNombre()));
        tIdentificacion.setCellValueFactory(new PropertyValueFactory<Item, String>("identificacion"));

        listItems.setItems(items);
    }

    public void actualizarSubastas() {
        try {
            ArrayList<Subasta> finalSubastas = new ArrayList<>();
            for (Subasta tmpSubasta: gestor.listarSubastas()) {
                if (!tmpSubasta.getEstadoSubasta().equals("cerrada")) {
                    finalSubastas.add(tmpSubasta);
                }
            }
            subastas = FXCollections.observableArrayList(finalSubastas);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tCodigo.setCellValueFactory(new PropertyValueFactory<Subasta, String>("identificacion"));
        tItem.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getItem().getNombre()));
        tOfertante.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getOfertante()));
        tOferta.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecioOferta()));
        tOfertas.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNOfertas()));

        listSubastas.setItems(subastas);
    }

    @FXML
    private void seleccionarSubasta(MouseEvent e) {
        Subasta subastaSeleccionada = listSubastas.getSelectionModel().getSelectedItem();
        Navigation.setSubastaSeleccionada(subastaSeleccionada);
        goToViewSubasta(e);
    }

    /**
     * Ir a vista ver Subasta
     * @param event
     */
    public void goToViewSubasta(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("subastaview.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Cerrar sesion
     * @param event
     */
    public void cerrarSesion(Event event) {
        gestor.cerrarSesion();
        backToHome(event);
    }

    /**
     * Ir a vista crear nueva Subasta
     * @param event
     */
    public void goToNewSubasta(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                Parent root = FXMLLoader.load(getClass().getResource("newsubasta.fxml"));
                Scene scene = new Scene(root);
                Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                appStage.setScene(scene);
                appStage.toFront();
                appStage.show();
            } else {
                showAlert(Alert.AlertType.ERROR, "Error!", "Debe iniciar sesion para crear una subasta");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ir a vista crear nuevo item
     * @param event
     */
    public void goToNewItem(Event event) {
        try {
            Usuario usuarioLogueado = gestor.checkearUsuarioLogueado();
            if (usuarioLogueado != null) {
                Parent root = FXMLLoader.load(getClass().getResource("newitem.fxml"));
                Scene scene = new Scene(root);
                Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                appStage.setScene(scene);
                appStage.toFront();
                appStage.show();
            } else {
                showAlert(Alert.AlertType.ERROR, "Error!", "Debe iniciar sesion para agregar un nuevo item");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
