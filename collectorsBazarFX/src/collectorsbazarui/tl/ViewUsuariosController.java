package collectorsbazarui.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import lopez.saul.bl.entities.imagen.Imagen;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.usuario.Usuario;
import lopez.saul.bl.logic.Gestor;

import java.time.LocalDate;
import java.util.ArrayList;

public class ViewUsuariosController {

    Gestor gestor;

    public ObservableList<Usuario> usuarios;
    @FXML
    TableView<Usuario> listUsuarios;
    @FXML
    private TableColumn<Usuario, String> tNombre;
    @FXML
    private TableColumn<Usuario, String> tCorreo;
    @FXML
    private TableColumn<Usuario, String> tDireccion;
    @FXML
    private TableColumn<Usuario, Integer> tTipoUsuario;
    @FXML
    private TableColumn<Usuario, LocalDate> tFechaNacimiento;
    @FXML
    private TableColumn<Usuario, Integer> tEdad;
    @FXML
    private TableColumn<Usuario, String> tContrasenia;
    @FXML
    private TableColumn<Usuario, String> tEstado;
    @FXML
    private TableColumn<Usuario, Double> tPuntuacion;

    /**
     * Obtiene el arraylist de usuarios para mostrarlo en rl tableview
     */
    @FXML
    public void initialize() {
        this.gestor = new Gestor();

        try {
            usuarios = FXCollections.observableArrayList(gestor.listarUsuarios());
        } catch (Exception e) {
            e.printStackTrace();
        }

        tNombre.setCellValueFactory(new PropertyValueFactory<Usuario, String>("nombre"));
        tCorreo.setCellValueFactory(new PropertyValueFactory<Usuario, String>("correo"));
        tDireccion.setCellValueFactory(new PropertyValueFactory<Usuario, String>("direccion"));
        tTipoUsuario.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("tipoUsuario"));
        tFechaNacimiento.setCellValueFactory(new PropertyValueFactory<Usuario, LocalDate>("fechaNacimiento"));
        tEdad.setCellValueFactory(new PropertyValueFactory<Usuario, Integer>("edad"));
        tContrasenia.setCellValueFactory(new PropertyValueFactory<Usuario, String>("contrasenia"));
        tEstado.setCellValueFactory(new PropertyValueFactory<Usuario, String>("estado"));
        tPuntuacion.setCellValueFactory(new PropertyValueFactory<Usuario, Double>("puntuacion"));

        listUsuarios.setItems(usuarios);
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("homefx.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
