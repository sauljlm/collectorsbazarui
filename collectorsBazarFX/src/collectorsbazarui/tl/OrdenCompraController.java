package collectorsbazarui.tl;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lopez.saul.bl.entities.item.Item;
import lopez.saul.bl.entities.ordencompra.OrdenCompra;
import lopez.saul.bl.logic.Gestor;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class OrdenCompraController {
    Gestor gestor;
    OrdenCompra ordenCompra;
    @FXML
    private Text nombre, fechaCompra, precio, detalle, codigo;

    public ObservableList<Item> items;
    @FXML
    TableView<Item> listItems;
    @FXML
    private TableColumn<Item, String> tNombreItem;
    @FXML
    private TableColumn<Item, String> tDescripcion;
    @FXML
    private TableColumn<Item, String> tEstado;
    @FXML
    private TableColumn<Item, LocalDate> tFechaCompra;
    @FXML
    private TableColumn<Item, String> tAntiguedad;
    @FXML
    private TableColumn<Item, String> tCategoria;
    @FXML
    private TableColumn<Item, String> tIdentificacion;

    public void initialize() {
        gestor = new Gestor();
        ordenCompra = Navigation.getOrdenCompraSeleccionada();
        System.out.println(ordenCompra);
        actualizarDatos();
        actualizarItems();
    }

    public void actualizarDatos() {
        nombre.setText(ordenCompra.getNombre());
        detalle.setText("Ha ganado la subasta " + ordenCompra.getDetalle());
        precio.setText(String.valueOf(ordenCompra.getPrecio()));
        codigo.setText(ordenCompra.getIdentificacion());
        fechaCompra.setText(String.valueOf(ordenCompra.getFecha()));
    }

    public void actualizarItems() {
        try {
            ArrayList<Item> itemsData = Navigation.getSubastaOrdenCompra().getAllItems();
            items = FXCollections.observableArrayList(itemsData);
        } catch (Exception e) {
            showAlert(Alert.AlertType.ERROR, "Error en el servidor!", "No se encontraron Items");
        }

        tNombreItem.setCellValueFactory(new PropertyValueFactory<Item, String>("nombre"));
        tDescripcion.setCellValueFactory(new PropertyValueFactory<Item, String>("descripcion"));
        tEstado.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstado().getNombre()));
        tFechaCompra.setCellValueFactory(new PropertyValueFactory<Item, LocalDate>("fechaCompra"));
        tAntiguedad.setCellValueFactory(new PropertyValueFactory<Item, String>("antiguedad"));
        tCategoria.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCategoria().getNombre()));
        tIdentificacion.setCellValueFactory(new PropertyValueFactory<Item, String>("identificacion"));

        listItems.setItems(items);
    }

    /**
     * Envia una alerta a la vista del fx
     * @param alertType
     * @param title
     * @param message
     */
    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Ir a vista inicio
     * @param event
     */
    public void backToHome(Event event) {
        try {
            gestor.eliminarOrdenCompra(ordenCompra.getIdentificacion());
            Parent root = FXMLLoader.load(getClass().getResource("coleccionistadashboard.fxml"));
            Scene scene = new Scene(root);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.toFront();
            appStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
